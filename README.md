# m346: Cloudlösungen konzipieren und realisieren



## Übersicht

In diesem Modul erarbeiten Sie sich teils geführt, teils selbstständig Kompetenzen. 

## Unterlagen

- [Informationen zu den Abgaben](./Abgaben.md)
- [Infrastructure As Code](./InfrastructureAsCode.md)
- [Cloud Computing](./CloudComputing.md)
- [SSH und Private/Public Keys](SshPublicPrivateKey.md)

![overview](./x_gitres/overview.png)

## Leistungsmessung

Die Kompetenzen werden gewichtet gewertet und jede Kompetenz trägt zur Gesamtnote bei.
