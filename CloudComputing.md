# Cloud Computing

[TOC]

### Virtualisierung

"A hypervisor is a process that separates a computer's operating system and applications from the underlying physical hardware. Usually done as software although embedded hypervisors can be created for things like mobile devices.

The hypervisor drives the concept of virtualization by allowing the physical host machine to operate multiple virtual machines as guests to help maximize the effective use of computing resources such as memory, network bandwidth and CPU cycles." (networkworld.com)

Graphisch darstellen, kann man dies wie folgt.

![Hypervisor](./CloudComputing/Hypervisor.png)

Sie sehen hier, dass es zwei Typen von Hypervisors gibt:

- Typ 1 (native): Läuft direkt auf der Hardware des Hostsystems.
- Typ 2 (hosted): Läuft als Applikation auf dem Betriebssystem.



### Hyperscaler

"Hyperscalers are large cloud service providers, that can provide services such as computing and storage at enterprise scale." (redhat.com)

"Hyperscalers get their name from hyperscale computing, a method of processing data that allows for software architecture to scale and grow as increased demand is added to the system." (redhat.com)

Vorstellen kann man sich dies gut, wenn man die Technologie des Hypervisors verwendet und diese vervielfacht. Natürlich werden hier nur Typ 1 Hypervisors verwendet, wegen der Performance.

![Hypervisor](./CloudComputing/Hyperscaler.png)

Das Grundprinzip ist bei alle grossen Anbietern das gleiche. Die Details können sich natürlich unterscheiden. 

[![Azure Hyperscaler](https://img.youtube.com/vi/KXkBZCe699A/0.jpg)](https://www.youtube.com/watch?v=KXkBZCe699A)



### Betriebsmodelle

Wir unterscheiden grundsätzlich die folgenden Betriebsmodelle:

- On Premise. Alle ihre Hard- und Software werden von Ihnen betrieben in eigenen oder gemieteten Datenzentren. 
- Hybrid Cloud. Sie haben ihre Server und Applikationen teilweise On Premise und teilweise in der Cloud.  Heutzutage trifft man dieses Modell am Öftesten an. Sobald eine Firma Office365 Lizenzen betreibt, sind zumindest Teile der Infrastruktur in der Cloud, auch wenn proprietäre Software noch auf eigenen Servern betrieben wird.  
- Cloud-Native. In diesem Modell ist die komplette Infrastruktur und alle Applikationen in der Cloud. 

Cloud Betriebsmodelle können zusätzlich in **Public Cloud** (öffentliche Anbieter wie Azure und AWS) und **Private Cloud** (Firmeneigene Cloud Infrastruktur) unterteilt werden. 

### Servicemodelle

Bei den Servicemodellen geht es darum zu unterscheiden, welcher Art die Cloudnutzung ist. 

- Infrastruktur as a Service (**IaaS**): Dieses Servicemodell umfasst die Bereitstellung der Hardware (z.B. virtuelle Maschinen oder Netzwerk) in der Cloud. Sie mieten Rechenleistung, Speicher, etc und installieren auf diesen Instanzen ihre Software.  Beispiele dazu sind Microsoft Azure, Google Compute Engine (GCE), Amazon Web Services (AWS), Rackspace, Cisco Metapod
- Plattform as a Service (**PaaS**): In diesem Modell wird eine Plattform in der Cloud zur Verfügung gestellt, auf der Sie ihre Software, Service, etc anbieten können. Sie verwenden dabei die Infrastruktur und SDKs der Hersteller und konzentrieren sich auf die Entwicklung der Software und nicht auf die Infrastruktur dahinter. Beispiele dazu sind AWS Elastic Beanstalk, Windows Azure, Heroku, Force.com, Google App Engine, Apache Stratos, OpenShift.
- Software as a Service (**Saas**): In diesem Modell wird die Software als Service in der Cloud gehostet. Sie verwenden die Software ohne, dass Sie sie lokal installieren müssen. Jede Webseite auf der Sie sich einloggen dient als Beispiel für SaaS (z.B. Google Workspace, Dropbox, Salesforce, Microsoft 365, etc)
- Function as a Service (**FaaS**)/**Serverless**: Ist ähnlich wie PaaS. Während bei PaaS Infrastruktur für Sie bereit gestellt wird, wird in FaaS die Infrastruktur für Sie auch verwaltet. Der grosse Vorteil bei FaaS sind die reduzierten Kosten. Nur wenn ihre *Function* auch ausgeführt wird, fallen Kosten an. 

![Servicemodelle](./CloudComputing/Servicemodelle.png)

### Cloud-Migrations-Modelle

Im Allgemeinen werden die folgenden Migrationsmodelle unterschieden:

- Rehosting (Lift & Shift): Dabei wird eine bestehende On-Premise Applikation genommen und mit minimalen Anpassungen in der Cloud wieder in Betrieb genommen. Dabei wird auf das Servicemodell **IaaS** aufgesetzt. Die Applikation wird auf virtuellen Servern installiert.
- Replatforming (Lift & Reshape): Ihre On-Premise Applikation wird soweit angepasst, dass Sie hinsichtlich der Infrastruktur optimal gehostet werden kann. z.B. kann eine Applikation mit ein paar Änderungen mit dem **PaaS** Servicemodell gehostet werden.
- Refactoring / Replace: Dabei schreiben Sie Ihre Applikation so um, dass Sie optimal mit den Cloud Services betrieben werden kann. Normalerweise wechseln Sie hier auf **Microservices** betrieben mit **Kubernetes** oder auf das **FaaS** Modell. Natürlich sind Mischformen möglich.
- Repurchasing: Möglicherweise evaluieren Sie die Marktlage und lösen Ihre bestehende Applikation mit einer bestehenden **SaaS** Lösung ab.

![Cloud Migrationsmodelle](./CloudComputing/Migrationsmodelle.png)

### Speichermodelle

Grundsätzlich kann man in die folgenden Modelle unterteilen. Es gibt natürlich Variationen im Detailierungsgrad und auch unterschiedliche Namensgebungen findet man.

- **Hot** Storage: Bezeichnet Speicher, welcher häufig aufgerufen wird und nur sehr kleine Verzögerungen erlaubt.
- **Warm** Storage: Bezeichnet Speicher, welcher regelmässig aufgerufen wird. Eine andere Bezeichnung ist z. B. "Cool Storage".
- **Cold** Storage: Bezeichnet Speicher, welcher sehr selten aufgerufen wird. Eine andere Bezeichnung ist z.B. "Archive Storage". 
- **Datenbank**: Datenbanken eben!

Während Betriebssysteme und Applikationen *Hot Storage* benötigen für die Laufzeit, werden Daten meistens in Datenbanken ausgelagert. *Warm Storage* kommt, z. B. für temporäre Dateien in Frage oder Videos und Audios, die gelegentlich abgespielt werden. *Cold Storage* kann gut für Backups verwendet werden. 

Schematisch kann man die Verwendung wie folgt abbilden.

![Storage](./CloudComputing/Storage.png)

### Netzwerk

Ein Netzwerk in einer public cloud kann mit dem folgenden Bild vereinfacht visualisiert werden.

![Netzwerk](./CloudComputing/Netzwerk.png)

Die wichtigen Begriffe kurz erklärt:

- Virtual Private Cloud (VPC): Dies bezeichnet ihr eigenes internes Netzwerk innerhalb des Anbieters. Im dargestellten Beispiel handelt es sich um das Subnetz 172.1.0.0/16. 
- Subnet: Ein Subnet bezeichnet ein Subnetz innerhalb der VPC (also eigentlich ein Subnetz eines Subnetz). Alle ihre virtuellen Server werden in Subnetzen platziert - entweder automatisch oder von Ihnen systematisch. 
- Gateway: Steht hier stellvertretend für das Routing zwischen dem Internet und den privaten Subnetzen. 

Das folgende Bild zeigt **konzeptionell** die Stationen, die eine Anfrage durchlaufen muss. 

![Sicherheit](./CloudComputing/Sicherheit.png)

Alle Stationen tragen zur Sicherheit im Netzwerk bei und werden folgend beschrieben:

**Security Group**: Die Security Group ist im Prinzip eine NAT Firewall, die Anfragen auf spezifischen Ports blockiert oder weiterleitet. Sie können bei den Regeln das Netzwerk einschränken und so nur interne Abfragen erlauben.

**Netzwerk-Adapter**: Eine virtuelle Instanz hat grundsätzlich zwei virtuelle Netzwerk-Adapter - eine für das private Subnetz und eine öffentliche IP. 

**Server Firewall**: Ihr Server hat grundsätzlich eine Firewall, die aktiviert sein kann. Die public cloud images haben die lokale Firewall jeweils **deaktiviert**.

**Pakete**: Die Softwarepakete haben oft noch zusätzlichen Schutz. Die MariaDB lässt per Standard nur Anfragen des localhosts (127.0.0.1) zu. Sie könnten alle Firewalls öffnen und kämen trotzdem nicht auf die Datenbank mit einer Fernabfrage.

Das folgende Bild zeigt das gleiche Szenario wie man es praktisch umsetzen würde. Dem Datenbankserver kann man (nach der Installation) die öffentliche IP entfernen und nur noch Anfragen über das lokale Netzwerk erlauben. Die lokalen Firewalls bleiben deaktiviert. 

![Sicherheit2](./CloudComputing/Sicherheit2.png)



### Skalierung

In den Quellen finden Sie einen Artikel über die vertikale und horizontale Skalierung und Load Balancing. Lesen Sie sich ein.

#### Vertikale Skalierung (Scale Up)

Bei der vertikalen Skalierung erhöht man die Ressourcenzuteilung einer virtuellen Instanz (oder eines physischen Rechners). Dies kann z.B. mehr RAM, höhere Anzahl CPUS oder mehr Speicherplatz bedeuten. 

Abhängig von dem darunterliegenden System kann dies "Hot" geschehen, also während dem laufenden Betrieb. VMWare unterstützt die Änderung von CPU und RAM "Hot", während bei public cloud Anbieter wie AWS oder Microsoft Azure die Instanz zuerst gestoppt werden muss. Der Grund dafür ist die Art und Weise wie verrechnet wird.

Der Datenspeicher kann auch bei den public cloud Anbieter im laufenden Betrieb angepasst werden.

#### Horizontale Skalierung (Scale Out)

Bei der horizontalen Skalierung ändern Sie nicht ihre Instanz, aber fügen weitere Instanzen hinzu, so dass die Last auf mehrere Instanzen verteilt wird. Vom Prinzip her funktioniert das ähnlich wie beim Hyperscaler bei dem Sie mehrere Rechner zusammenhängen und dann die Ressourcen von aussen als eine Einheit erscheinen lassen. 

Auf diese Weise können Sie jederzeit einfach weitere Instanzen hinzufügen, ohne, dass ihre Applikation/Ihre Umgebung heruntergefahren werden muss.

### Load Balancer

Ein Load Balancer wird benötigt, um die Last auf mehrere Server zu verteilen, bei einer horizontalen Skalierung. Im einfachsten Fall, verteilt der Load Balancer jede Anfrage abwechselnd an die verschiedenen Instanzen. Ein Load Balancer kann aber auch intelligentere Logik enthalten und die Anfragen an die am wenigsten ausgelasteten Instanzen verteilen.

#### Auto Scaling

Auto Scaling bezeichnet den Vorgang, wenn automatisch neue Instanzen hinzugefügt und wieder entfernt werden, abhängig von der Auslastung der bestehenden Server. Auf diese Weise kann ihre Applikation jederzeit optimal antworten.

![AutoScaling](./CloudComputing/AutoScaling.png)



### Quellen

networkworld.com: [What is a hypervisor?](https://www.networkworld.com/article/3243262/what-is-a-hypervisor.html)

redhat.com: [What is a Hyperscaler](https://www.redhat.com/en/topics/cloud/what-is-a-hyperscaler)

ionos.de: [Serverless Computing: Das steckt hinter dem modernen Cloud-Modell](https://www.ionos.de/digitalguide/server/knowhow/serverless-computing/)

AWS I: [Storage](https://docs.aws.amazon.com/whitepapers/latest/aws-overview/storage-services.html)

Microsoft: [Hot, cool and archive access tier for blob data](https://learn.microsoft.com/en-us/azure/storage/blobs/access-tiers-overview)

missioncloud.com: [Horizontal Vs. Vertical Scaling: Which Is Right For Your App?](https://www.missioncloud.com/blog/horizontal-vs-vertical-scaling-which-is-right-for-your-app)

aws.amazon.com: [What Is Load Balancing?](https://aws.amazon.com/what-is/load-balancing/)
